package az.ingress.hellospring.model.mapper;

import az.ingress.hellospring.model.dto.StudentDto;
import az.ingress.hellospring.model.entity.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface StudentMapper {


    Student toStudent(StudentDto studentDto);

    StudentDto toStudentDto(Student studentEntity);

    List<StudentDto> toStudentDtos(List<Student> students);

    @Mapping(source = "studentId", target = "id")
    Student toStudent(StudentDto studentDto, Long studentId);
}
