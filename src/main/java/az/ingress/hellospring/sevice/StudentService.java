package az.ingress.hellospring.sevice;

import az.ingress.hellospring.model.dto.StudentDto;
import az.ingress.hellospring.model.entity.Student;
import az.ingress.hellospring.model.mapper.StudentMapper;
import az.ingress.hellospring.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentMapper mapper;
    private final StudentRepository studentRepository;


    public List<StudentDto> getStudents() {
        return mapper.toStudentDtos(studentRepository.findAll());
    }

    public StudentDto getStudent(Long id) {
        Student student = studentRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Student not found"));
        return mapper.toStudentDto(student);
    }

    public StudentDto createStudent(StudentDto studentDto) {
        return mapper.toStudentDto(studentRepository.save(mapper.toStudent(studentDto)));
    }

    public StudentDto updateStudent(Long id, StudentDto studentDto) {
        Optional<Student> studentEntity = studentRepository.findById(id);
        if (studentEntity.isPresent()) {
            return mapper.toStudentDto(studentRepository.save(mapper.toStudent(studentDto , id)));
        }
        return createStudent(studentDto);
    }

    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }


}
