package az.ingress.hellospring.tictactoe;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class TicTacToeTest {

    private TicTacToe ticTacToe;

    @BeforeEach
    void init() {
        ticTacToe = new TicTacToe();
    }

    @Test
    void whenPieceIsOutsideTheBoardThenException() {

        int x = 2;
        int y = 5;

        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void whenXIsOutsideTheBoardThenException() {

        int x = -1;
        int y = 0;

        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }

    @Test
    void whenYIsOutsideTheBoardThenException() {

        int x = 1;
        int y = 4;

        assertThatThrownBy(() -> ticTacToe.play(x, y))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        ticTacToe.play(2, 1);

        assertThatThrownBy(() -> ticTacToe.play(2, 1))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");

    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO() {
        ticTacToe.play(1, 1);
        assertThat(ticTacToe.nextPlayer()).isEqualTo('O');
    }

    @Test
    public void givenLastTurnWasOWhenNextPlayerThenX() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void whenPlayThenNoWinner() {
        String actual = ticTacToe.play(1, 1);
        assertThat(actual).isEqualTo("No winner");
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O
        String actual = ticTacToe.play(3, 1); // X
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        String actual = ticTacToe.play(1, 3); // O
        assertThat(actual).isEqualTo("O is the winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 3); // O
        String actual = ticTacToe.play(3, 3); // O
        assertThat(actual).isEqualTo("X is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);
        String actual = ticTacToe.play(3, 2);
        assertThat(actual).isEqualTo("The result is draw");
    }
}

